package finals;

//You can not subclass PrintedBook because that class is final

// This does not work:
//public class PocketVersionBook extends PrintedBook {
//    public PocketVersionBook(String title, String author, int pages) {
//        super(title, author, pages);
//    }
//}
