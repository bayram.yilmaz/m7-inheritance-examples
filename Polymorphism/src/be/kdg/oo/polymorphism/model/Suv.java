package be.kdg.oo.polymorphism.model;

public class Suv extends Car {
    private boolean allWheelDrive;

    public void toggleAllWheelDrive() {
        this.allWheelDrive = !this.allWheelDrive;
    }

    @Override
    public void drive() {
        if (!allWheelDrive) {
            super.drive();
        } else {
            System.out.println("Take me home, country roads...");
        }
    }
}
