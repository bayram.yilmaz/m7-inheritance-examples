package be.kdg.oo.polymorphism.model;

public abstract class Vehicle {
    public abstract void drive();
}
