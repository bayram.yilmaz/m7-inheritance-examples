package be.kdg.oo.polymorphism.model;

public class Car extends Vehicle {
    @Override
    public void drive() {
        System.out.println("Get your motor runnin', Head out on the highway...");
    }
}
