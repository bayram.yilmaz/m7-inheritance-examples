package composition;

/**
 * Author: Jan de Rijke
 */
public class Phone {
	String brand;

	public Phone(String brand) {
		this.brand = brand;
	}
}
