package be.kdg.oo.abstracts.model;

public class Book extends Product {
    public Book(String code, int price) {
        super(code, price);
    }

    @Override
    public void promote() {
        System.out.println(String.format("Book %s is now on sale! Get it for only %d", code, price));
    }
}
