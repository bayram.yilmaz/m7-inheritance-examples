package be.kdg.oo.abstracts.model;

public abstract class Product {
    protected String code;
    protected int price;

    public Product(String code, int price) {
        this.code = code;
        this.price = price;
    }

//    Concrete classes must implement this method
//    You only need to supply the method signature  ( naam + parameters),access modifier  ,abstract keyword and  returntype
//    Do not forget the semicolon at the end
    public abstract void promote();

    public final void sell() {
        System.out.println("Selling product " + code);
    }
}
