package shape;


import shape.geometry.*;

/**
 * Author: Jan de Rijke
 */
public class ShapeApp {
	public static void main(String[] args) {
		Shape[] shapes = new Shape[2];
		shapes[0] = new Rectangle(7,3);
		shapes[1] = new Circle(8);
		showSurfaces(shapes);
	}

	public static void showSurfaces(Shape[] shapes){
		for (Shape shape:shapes) {
			System.out.println(shape.getArea());
		}
	}
}
