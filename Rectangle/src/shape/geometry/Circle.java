package shape.geometry;

public class Circle extends Shape{
    /*
     * Instance data member(s)
     */

    private int radius;
    
    /*
     * Constructor(s)
     */
    // Default constructor
    public Circle() {
        /*
         Constants cannot be defined here since first statement must be
         a call to a constructor
            final int MIN_X = 0;
            final int MIN_Y = 0;
            final int MIN_RADIUS = 1;
            this(MIN_X, MIN_Y, MIN_RADIUS);	// Solution: see M8 (static)
         */
        this(0, 0, 1);
    }
    
    public Circle(int radius) {
        /*
         Constants cannot be defined here since first statement must be
         a call to a constructor
            final int MIN_X = 0;
            final int MIN_Y = 0;
            this(MIN_X, MIN_Y, radius);	// Solution: see M8 (static)
         */
        this(0, 0, radius);
    }
    
    public Circle(int x, int y, int radius) {
        setX(x);
        setY(y);
        setRadius(radius);
    }
    
    // Copy constructor
    public Circle(Circle shape) {
        this(shape.getX(), shape.getY(), shape.getRadius());
    }
    

    
    public int getRadius() {
        return radius;
    }
    
    public void setRadius(int radius) {
        final int MIN_RADIUS = 1;
        final int MAX_RADIUS = 1000;
        
        if ((radius >= MIN_RADIUS) && (radius <= MAX_RADIUS)) {
            this.radius = radius;
        } else {
            System.out.printf("Invalid radius %d [%d..%d] \n", radius, MIN_RADIUS, MAX_RADIUS);
        }
    }
    
    public void setPosition(int x, int y) {
        setX(x);
        setY(y);
    }
    
    /*
     * Method(s)
     */
    public void grow(int delta) {
        setRadius(radius + delta);
    }
    
    public double getArea() {
        return Math.PI * Math.pow(radius, 2);
    }
    
    public double getPerimeter() {
        return 2 * Math.PI * radius;
    }
    
    public String toString() {
//        return String.format("Circle with radius %d located at (%d,%d); area=%-8.02f perimeter=%-8.02f", radius,  x, y,  getArea(), getPerimeter());
        return super.toString() + String.format(" radius %d area=%-8.02f   perimeter=%-8.02f",  radius, getArea(), getPerimeter());
    } // toString()
}  // class
