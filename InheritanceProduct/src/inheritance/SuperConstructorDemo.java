package inheritance;

import inheritance.model.Book;

/**
 * Author: Jan de Rijke
 */
public class SuperConstructorDemo {
	public static void main(String[] args) {
		// prints out message from superclass constructor
		Book mistBorn = new Book();
	}
}
