package inheritance.model;

import java.text.NumberFormat;
import java.util.Locale;

public class Product {
	private String code;
	private String description;
	private double price;

	public Product() {
		System.out.println("First init a product");
	}

	public Product(String code, String description, double price) {
		this.code = code;
		this.description = description;
		this.price = price;
	}

	public String getCode() {
		return code;
	}

	private void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	protected void setDescription(String description) {
		this.description = description;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String toString() {
		return String.format("%-8s , %-30s , %-6.2f Eur"
			, getCode(), getDescription(), getPrice());
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Product)) {
			return false;
		}
		Product other = (Product) obj;
		return (getCode() != null ? getCode().equals(other.getCode()) : other.getCode() == null);
	}

	@Override
	public int hashCode() {
		return getCode() != null ? getCode().hashCode() : 0;
	}
}
