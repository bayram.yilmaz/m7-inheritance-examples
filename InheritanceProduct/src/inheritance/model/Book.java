package inheritance.model;

//Book is a kind of Product
public class Book extends Product {
    private String author;
    private String title;

//    Multiple constructor parameters, even though Book only has 2 attributes
    public Book(String code, String description, double price, String author, String title) {
//        Don't forget to cal the superclass constructor! Pass the common attributes
        super(code, description, price);
        this.author = author;
        this.title=title;
    }

	public Book() {
	}



    public String getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        return super.toString() + String.format(" author:%-15s  title:%s", author, title);
    }  // toString()

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
