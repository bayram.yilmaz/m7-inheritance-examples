package inheritance;

import inheritance.model.*;


public class InstanceofDemo {
    public static void main(String[] args) {
	    Product item1 = new Book("123","IT",20.0,"Deitel","Java");
	    Product item2 = new Camera("456","digital",160.0,8_000_000);

			// inheritance and assignment
	   // Book deitel = item1; // compiler error
	    Book deitel = (Book) item1; // OK
	   // Book camera = (Book) item2; // Compiles OK, runtime ClassCastException
	    System.out.println(item1 instanceof Product);
			System.out.println(item1 instanceof Object);

	    System.out.println("OK: "+ item1.getPrice());
	    //System.out.println("Compiler error: "+item1.author);
	    System.out.println("OK: "+((Book)item1).getAuthor());

	    Book b = new Book();
	    System.out.println(b instanceof Object);
	    System.out.println(b instanceof Book);
	    System.out.println(b instanceof Comic);
//	    System.out.println(b instanceof Camera);
//        De compiler does not allow you to check this,
//        It knows this is impossible because Camera is not in the Book hierachy

	    Book book = new Comic("789","Lucky Luke",10.0,"Goscinny","Ma Dalton","Morris");
	    if (book instanceof Comic c) {
		    System.out.println("Illustrations by " + c.getArtist());
	    }


			slogan(book);

	    System.out.println("Look: a " + item1.getClass().getSimpleName());

    }

		public static void slogan(Product p) {
			switch (p) {
				case Book b -> System.out.println("The new sensation: " + b.getTitle());
				case Camera c -> System.out.println("Pixels galore: " + c.getPixels());
				case Software s -> System.out.println("Upgrade to the latest and greatest version: " + s.getVersion());
				case null -> System.out.println("Sold out!");
				default -> System.out.println("Unknown or basic product");
			}
		}
}
